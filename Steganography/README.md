## Yellow Forest.wav
This file contains a secret file hidden away using AES encryption. You'll need a password to solve this one.

**Difficulty: Intermediate** 

## savage_patrick.bmp
In this picture, there's a text file with the flag in it. Can you find the flag behind this picture?

**Difficulty: Easy**