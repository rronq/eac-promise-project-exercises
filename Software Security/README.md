## Hex Editing 101

**Scenario:** You've been given an executable file that asks for a password. Unfortunately, you don't have a password but checking the hex values might give you a little hint. Once you find the flag,
you'll need to further decrypt it into plaintext. 

**Difficulty: Intermediate**

## Crack This!
**Scenario:** You've found several binary files that ask for either a validation code or license key. Perhaps the secret lies within the program itself?

**Note:** Compile and build the code first and attempt to solve it using reverse engineering methods without looking at the source code.

**Difficulty: Easy to Hard**