## ftpLab.pcap
**Scenario:** You've been given an FTP packet capture and your goal is to download the hidden flag. When doing this lab, follow the TCP stream and look at some of the things captured like usernames 
and passwords.

**Difficulty:** Easy


## telnetLab.pcap
**Scenario:** This exercise doesn't have a flag and is more of a packet analysis test. When looking through this capture, think about and answer some of the following questions:

1. What is the username and password?
2. What command did the attacker run after authenticating?
3. What flavor of Linux and what version was running on the victim?

**Difficulty:** Easy